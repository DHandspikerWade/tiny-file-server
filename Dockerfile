FROM golang:1.17-alpine AS builder
COPY . .
RUN go build -o server server.go

FROM alpine
COPY --from=builder /go/server ./server
VOLUME  /webroot
CMD ["./server"]